---
title: 'Belohnungen'
bgcolor: '#f2f2f2ff'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: "Spende 30 €"
        text: "Erhalte eine persönliche Neujahrs-Grußkarte."
    -
        title: "Spende 75 €"
        text: "Eine Neujahrs-Spezialüberraschung ist auf dem Weg in Deine Mailbox."
    -
        title: "Spende 100 €"
        text: "Wähle für zwei Wochen ein neues Bild für die Hauptseite von Disroot.org.*"
    -
        title: "Spende 500 €"
        text: "Wähle einen neuen Domainnamen für ein benutzerdefiniertes Alias, das allen Disrootern zur Verfügung gestellt wird.*"


---

<div markdown=1>
`*` Disroot.org behält sich das Recht vor die Belohnung abzulehnen, wenn das Bild oder der Name unsere <a href="/tos"> TOS</a> verletzen.
</div>
