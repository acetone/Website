---
title: 'New roadmap 2020'
media_order: road.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - roadmap
body_classes: 'single single-post'
---


Hey all Disrooters! Are you all well, rested and prepared for the new year? We sure are, and can't wait for what is coming our way. We want to start the year by planning and deciding what we want to be busy with in the months to come.

## The 2020 plan

As always, this time we are also having some ambitious plans for this year. We have identified few themes we wanted to focus on.

1.  **Email improvements**

    We want to focus on a number of improvements of our email service. Starting from new and better webmail solution, bigger promotion of native email clients (apps) and focus on full support of Autocrypt in webmail as a promotional campaign of Autocrypt (easy user/app side independent end-to-end email encryption) not only for users but most importantly mail client developers, to stive for vider addoption of the solution. As we are getting more 'popular' we are more often attacked by spam, phishing and other nasty things ruining email experience. We want to focus on better anti-spam/virus solutions too.

2.  **Mailbox encryption**

    One of the things we have not managed to achieve last year is the mailbox encryption. This year we hope we will finally nail it. Initially we thought of server side encryption (where your private keys are stored on the server), however recently we started talking more and more about possibility to utilize GnuPG (gpg/pgp) as a way to end-to-end encrypt emails at rest (mailbox encryption). We hope we will be able to come up with solution that not only will serve Disrooters, but since it would be fully open sourced would allow any email server provider to utilize. No more closed silos. If we manage time will tell. As a fallback solution we will be working on server side mailbox encryption.

3.  **Better administration**

    One of the themes of last roadmap for 4th quarter of 2019 we started looking into ways to improve administration and way we handle support tickets, custom requests and all sort of routine tasks that consume more and more of our time. We also do realize that some of the tasks facing all of you (additional storage, custom domains etc) do not feel professional and take definitely too much time to be processed. We want to address all those things and improve as much as we can, so that your requests will be processed quick and professional and for us, all the routine daily tasks will not feel like a chore and we will have more time to focus on fun stuff like improving services we run.

4. **Onboard more admins**

    For the last years Disroot was run by muppeth and antilopa who were responsible for everything Disroot (from website, daily tasks, administration, support, system administration, maintenance and all). Since last few months the Disroot team grew with additional force (fede and meaz in the core team and massimiliano and maryjane as volunteer close help). Already last roadmap we have started on defining and redistributing some of the tasks such as support tickets, translation project and howtos management, website building and so on. We want to continue this year by training and onboarding new core team members to allow them access to more production servers, to help with troubleshooting, maintenance and other tasks and requests that require production access to the servers.

5. **Launch Hubzilla**

    Some of you know we have fallen in love in Hubzilla two years ago. We think it is a brilliant piece of software ahead of its time with great potential, offering features no other network provides. The moment we discovered the genius of Hubzilla, we wanted it to become our number one go to social network. Hubzilla needs a lot of love, and a lot of work to become service we can provide as go to social network for Disroot. After last two years we have not managed to get our test instance to the point where we would be satisfied with it. Getting hubzilla to desired state as a goal and priority will help get to the point where we can provide hubzilla to all disrooters.

6. **Funding**

    Saying Disroot has not become our dayjob would be a lie. We spend on Disroot more time then we do on our actual day jobs. And since we need to maintain both, not to mention our families, other projects, hobbies and maybe some social life (which at this moment is not existing), we need to change this situation. Getting to the place where Disroot becomes our main occupation seems to be still far ahead, but we need to seriously start working towards it. Last year has exceeded our most enthusiastic expectations and showed us that the dream of financial independence is realistic. That is why we want to dedicate some time to explore ways how to make this dream come true.


## What's next ?

We have divided our plans into three month long roadmaps. Right now, we are busy working on laying the ground work for all the themes and we continue to work on tasks we have not managed to accomplish during the last months. Furthermore, we are refining all the plans in more detail to improve our working experience.
