---
title: 'The Phenomenal End of the Year Bonus Alias Challenge'
date: '23-12-2019 23:00'
media_order: front-page-image.jpg
taxonomy:
  category: news
  tag: [disroot, fundraising, domains, aliases]
body_classes: 'single single-post'

---

In the last days of this year, when the entire world prepares to greet the light of the new decade, three domains are fighting for the dominance of the beetroot market. Join the battle and choose your Overlord.
<br><br>
From 24th of December until 1st of January, take part in our end of the year fundraising event. Donate to Disroot using one of the below domain hashtags as your donation reference. Victorious domain name will be given as an extra alias for all Disrooters now and in the future.
<br><br>
**Let the coolest domain win!**

 - **\#glorifiedhacker** - glorifiedhacker.net
 - **\#GetGoogleOffMe** - getgoogleoff.me
 - **\#Morecats** - morecats.net

More info at: [https://disroot.org/aliaschallenge](https://disroot.org/aliaschallenge)
<br><br>

 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.co.uk/videos/embed/5a3cbc03-1831-4dee-b2cd-92d80dd51f2b" frameborder="0" allowfullscreen></iframe>
