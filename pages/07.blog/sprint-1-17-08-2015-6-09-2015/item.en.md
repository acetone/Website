---
title: Sprint 2 – New logo, donation page, backup system
date: 07/09/2015
taxonomy:
  category: blog
  tag: [disroot, news, donations, monitoring, backup]
body_classes: single single-post

---


# New logo, donation page, backup system

 This sprint have passed really fast. Mostly we did a lot of work on ‘behind the scenes’ functionality such as backup solution, and service monitoring. We also made a start with writing our internal documentation. Antilopa made awesome logo which we will implement in all services together with unifying the overall user experience during next few sprints.
Last but not least, we made it possible to receive donations \o/ By throwing money at us you help maintain the project as well as make it possible to grow so that more people can make use of it.

##New on Disroot

**Disroot Logo** – Yes we have a brand new logo thanks to Antilopa. In coming sprints it will be incorporated into all the services as a part of unifying all interfaces. We hope you like the new logo as much as we do. https://forum.disroot.org/uploads/default/original/1X/bcac8e8149cab628e12dc92d14d7e05dd6690e4b.png

**Donation page** – For all the people asking us where they can donate money, we finally have an answer: Here
**Email Server auto-detect settings** – Now your Thunderbird and Outlook (anyone with windows machine that could actually test this one for us?) should auto-magically detect our email server settings, so no need to remember the actual server address, port number etc.

**Internal Service Monitoring** – We’ve setup icinga2 to monitor the health of the servers as well as all the processes and services running on it. Whenever there is a problem we immediately get notified so that our response time is much shorter as we are aware of problems right away.

**Improved Backup solution** – We are now running backups to remote server. Backup server’s data disks are encrypted (as well as disroot server btw). Here is an overview of what and when we backup:

  - Daily user data and databases – we make daily backup of all user data and databases to remote server

  - Weekly rotate – the data collected during the week on backup server is then compared with the main server once a week and synchronized so that all the files deleted by user during that time from the server is then deleted on the backup machine as well, to prevent us from storing files we are not suppose to have anymore.

  - Monthly Operating systems backup. – we are doing monthly system upgrades of all of our virtual machines / containers running disroot services. Before and after this operation the operating systems are backed up locally (before system update), remotely (after successful system upgrade)

We are looking forward to our sprint 2 – 8.09.2015 – 30.09.2015
