---
title: 'Sprint 1 – First disroot sprint is closed'
date: 08/15/2015
taxonomy:
  category: news
  tag: [disroot, news, owncloud, roundcube, discourse, mail, chat]
body_classes: 'single single-post'

---

# New Features in mail, forum and owncloud
It’s been a quick week here at disroot. We’ve announced our project to diaspora community making it kind of official. We’ve received quite some positive feedback and constructive criticism for which we are very grateful. Not wasting too much time we jumped to work and fixed some things that we had on our list.

## Transparent Development

After the last meeting with the disroot crew we decided to put some structure to the way we work. We will organize our work in 3 week sprints. It means every three weeks we’ll plan work for coming period by picking tasks from our backlog and adding them to our board.

We want to be as open about things we do as possible, so we made our kanban board public. Here, you can follow the progress of each task that was planned for current sprint. This gives opportunity to everyone to be more familiar with what we do, give better (more) feedback but also maybe take responsibility for few tasks here and there and help us out.

## New Features

1. owncloud [https://cloud.disroot.org](https://cloud.disroot.org)
– added xmpp (chat) support. Now you can make use of webchat with otr encryption and chatroom functionality.

2. roundcube [https://mail.disroot.org](https://mail.disroot.org)

    added xmpp (chat) support.
    added sieve functionality. Now you can create rules to sort out your email automatically. Just go to “Settings” > “Filters” and “Add” filter rules. Those rules will apply to any and every mail client you are using.

3. discourse [https://forum.disroot.org](https://forum.disroot.org)
Now you can easily use discourse as both your mailing-list and forum:

    reply to the topic with email. Everytime you get notification on the topic you are following you can reply to it straight from the email.
    create topics via email – By sending mail to email alias assigned to your group you can create a topic. No need to login to the interface. Please contact admins if you want this functionality.
    To post to disroot category just send email to forum@disroot.org

4. mail

    POP3 – you can now use pop3 protocol for disroot email. Use port is 995 and enable SSL.
    Default folders – Draft, Inbox, Sent, Trash, Junk folders are created for every email account by default
    Sieve – Creating filters for your emails (currently exposed only to roundcube but affecting all mail clients)
    Spamassasin – To prevent you getting tons of spam we’ve put a guard to kill ’em before they reach you. Please let us know if you are getting emails not flagged as spam when they should have.
    ClamAV and amavis – Open source virus scanners for emails.

5. chat

    Added offline messaging support so you don’t miss anything when you are offline.
