---
title: 'Hostilidad de Microsoft'
media_order: power.jpg
published: true
date: '7-11-2020 12:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - microsoft
        - email
        - novedades
body_classes: 'single single-post'
---

Como algunxs de ustedes habrán notado, **Microsoft** (propietario de Hotmail, Outlook, Live, Office360, etc) está rechazando todos los correos provenientes de los servidores de **Disroot.org**. Una vez que nos informaron del problema, nos contactamos inmediatamente con el soporte en **Outlook** ya que no es la primera vez que somos blanco de **Microsoft** (el año pasado sin ninguna razón, todos los correos desde **Disroot** eran marcados como SPAM) .

Después de esperar durante varios días por alguna respuesta, asegurarnos que cumplíamos con 'sus políticas', e intercambiar algunos inútiles emails con el staff del soporte de **Microsoft**, obtuvimos la siguiente contestación:

> Hola,
> Como se dijo anteriormente, su IP (178.21.23.139) no califica para la mitigación en este momento. Me disculpo, pero no puedo dar detalles sobre esta situación ya que no tenemos la libertad de discutir la naturaleza del bloqueo.
> En este punto, le sugiero que revise y cumpla con las normas técnicas de Outlook.com. Esta información se puede encontrar en https://postmaster.live.com/pm/postmaster.aspx.
> Lamentamos no poder proporcionar ninguna información o ayuda adicional en este momento.
> Saludos cordiales"

Esto es como decir: "*Están bloqueados, no les diremos por qué, y todo lo que pueden hacer es esperar a que decidamos lo contrario y permitir que lleguen sus correos*". La vieja y conocida hostilidad de Microsoft.

**Lamentamos** que sus correos electrónicos no estén llegando a sus contactos en **Hotmail**, **Outlook** o **Live**. Buscando en la web los posibles motivos del bloqueo, descubrimos que es una práctica bastante común de MS hacer esto por largos períodos de tiempo y sin razón aparente.

Quizás si sus usuarias y usuarios empiezan a hacer preguntas, **Microsoft** finalmente revise las razones por las que nos han bloqueado, sea tan amable de explicarnos los motivos, o simplemente nos desbloqueen. Así que, **¡contacten a Microsoft!**. Sin embargo, descubrirán ¡qué difícil es ponerse en contacto! Una menera es a través de esta página:

[https://support.microsoft.com/es-es/contactus/](https://support.microsoft.com/es-es/contactus/)

**Por favor, tomen partido, involúcrense, republiquen y compartan esta entrada así podemos entre todos y todas llamar la atención de MS y revertir este bloqueo.**
