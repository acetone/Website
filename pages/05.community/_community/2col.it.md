---
title: 'Roadmap'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

# Comunity

---      

La **trasparenza** gioca un ruolo importante qui in Disroot.
Vogliamo essere aperti sui nostri progetti, idee e progressi attuali.
Vogliamo inoltre incoraggiare i contributi al progetto da parte della comunità.
Pertanto, abbiamo messo a punto una serie di strumenti per rendere più facile seguire lo sviluppo di Disroot. È inoltre possibile  partecipare attivamente alle discussioni, alla segnalazione di problemi e alla presentazione di idee e correzioni.
