---
title: privacy
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

<br>

Il software può essere creato e modellato in qualsiasi cosa. Ogni pulsante, ogni colore e ogni link che vediamo e usiamo sul web è stato messo lì da qualcuno. Quando usiamo le applicazioni che ci vengono fornite, in genere non vediamo - e a volte non ci interessa - molto di ciò che accade dietro l'interfaccia che usiamo. Ci mettiamo in contatto con altre persone, conserviamo i nostri file, organizziamo incontri e festival, mandiamo e-mail o chiacchieriamo per ore e tutto questo accade per magia.
Negli ultimi decenni le informazioni sono diventate molto preziose e sempre più facili da raccogliere ed elaborare. Siamo abituati ad essere analizzati, accettando ciecamente termini e condizioni per "il nostro bene", confidando nelle autorità e nelle aziende multimiliardarie per proteggere i nostri interessi, mentre noi siamo sempre il prodotto nelle loro " fattorie della gente".

**Possiedi i tuoi dati personali:**
Molte reti utilizzano i tuoi dati per fare soldi analizzando le tue interazioni e utilizzando queste informazioni per pubblicizzare le cose a voi. Disroot non utilizza i vostri dati per scopi diversi da quelli che vi permettono di connettervi e utilizzare il servizio.
I tuoi file sul cloud sono criptati con la tua password utente, ogni bin-paste e file caricati sul servizio Lufi è criptato anche lato client, il che significa che anche gli amministratori dei server non hanno accesso ai tuoi dati. Ogni volta che c'è la possibilità di crittografia, la abilitiamo e, se non è possibile, consigliamo di utilizzare un software di crittografia esterno. Come amministratori, meno noi conosciamo i vostri dati, meglio è :D. (Consiglio del giorno: non perdere mai la password!)  [guarda la guida](https://howto.disroot.org/it/tutorials/user/account/ussc)

---

# Privacy


![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
