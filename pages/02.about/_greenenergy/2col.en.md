---
title: Online-donation
bgcolor: '#1F5C60'
wider_column: left
fontcolor: '#ffffff'
---

Disroot servers are hosted in a data-center using renewable energy.

---

<a href="https://api.thegreenwebfoundation.org/greencheckimage/DISROOT.ORG" target=_blank><img src="https://api.thegreenwebfoundation.org/greencheckimage/DISROOT.ORG" alt="This website is hosted Green - checked by thegreenwebfoundation.org" style="height:100px"></a>
