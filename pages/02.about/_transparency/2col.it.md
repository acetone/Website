---
title: Transparency
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

# Trasparenza e apertura

![](about-trust.png)

---

<br>

Su **Disroot** usiamo software libero e open source al 100%. Questo significa che il codice sorgente (il modo in cui opera il software) è pubblicamente accessibile. Chiunque è in grado di apportare miglioramenti o modifiche al software, che può essere verificato e controllato in qualsiasi momento - senza backdoor nascoste o altro malware dannoso.

Vogliamo essere completamente trasparenti e aperti verso le persone che utilizzano i nostri servizi e per questo motivo pubblichiamo informazioni sullo stato attuale del progetto, la situazione finanziaria, i nostri progetti e le nostre idee. Vorremmo anche sentire i vostri suggerimenti e feedback in modo da poter offrire la migliore esperienza possibile a tutti gli utenti Dsiroot.
