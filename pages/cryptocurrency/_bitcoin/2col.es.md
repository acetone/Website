---
title: Bitcoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

![](qr0508.png)

---

# Bitcoin
Envía [bitcoins](https://es.wikipedia.org/wiki/Bitcoin) a nuestra cartera a:  
bc1qa9xy9zrcy52xccmjhlzunvd5y7dqst0qh4up8e
