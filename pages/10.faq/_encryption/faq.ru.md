---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "ШИФРОВАНИЕ"
section_number: "200"
faq:
    -
        question: "Я могу использовать двухфакторную авторизацию (2FA)?"
        answer: "<p>Да, вы можете использовать ее для <b>Облака</b>. Но перед тем, как включить его, убедитесь, что вы полностью понимаете, как он работает и как его использовать. Для получения дополнительной информации перейдите <a href='https://howto.disroot.org/ru/tutorials/cloud/introduction#two-factor-authentication' target='_blank'>сюда</a>.</p>"
    -
        question: "Могу ли я использовать сквозное шифрование в облаке?"
        answer: "<p>В настоящее время сквозное шифрование отключено из-за давней ошибки с приложением <b>Nextcloud</b>.</p>"


---
