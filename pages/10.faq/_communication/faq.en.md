---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMMUNICATION"
section_number: "100"
faq:
    -
        question: "I can't connect to Matrix. Why?"
        answer: "<p>We decided to discontinue <b>Matrix</b> since September, 2018. You can read the reasons <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>here</a>.</p>
        <p>You can also find more information about the privacy impact of Matrix <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>here</a></p>"
    -
        question: "How do I use Disroot's IRC in XMPP rooms?"
        answer: "<p><b>Disroot</b> provides so-called IRC gateway. This means you can join any IRC room hosted on any IRC server. This is the basic address schema:</p>
        <ol>
        <li>To join an IRC room: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>To add an IRC contact: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Where <b>#room</b> is the IRC room you want to join and <b>irc.domain.tld</b> is the irc server address you want to join. Make sure to leave <b>@irc.disroot.org</b> as it is because it's the IRC gateways address.</p>"
    -
        question: "I can’t connect to Diaspora* with my Disroot account."
        answer: "<p>Since <b>Diaspora*</b> doesn’t support <b>LDAP</b> (the software protocol we use to access services with a single account) it is not possible to login with <b>Disroot</b> credentials. You need to create a different account <a href='https://pod.disroot.org/' target='_blank'>here</a>. However, registrations are currently closed.</p>"

---
