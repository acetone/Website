---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "КОММУНИКАЦИИ"
section_number: "100"
faq:
    -
        question: "Я не могу присоединиться к Matrix. Почему?"
        answer: "<p>Мы решили прекратить поддержку <b>Matrix</b> в сентябре 2018 года. Вы можете прочесть о причинах <a href='https://disroot.org/ru/blog/matrix-closure' target='_blank'>здесь</a>.</p>
        <p>Вы также можете найти дополнительную информацию о влиянии Matrix на конфиденциальность <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>тут</a>.</p>"
    -
        question: "Как использовать IRC Disroot через XMPP?"
        answer: "<p><b>Disroot</b> предоставляет так называемый шлюз IRC. Это означает, что вы можете присоединиться к любой комнате IRC, размещенной на любом сервере IRC. Это основная схема адреса:</p>
        <ol>
        <li>Войти в IRC-комнату: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>Добавить IRC-контакт: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Где <b>#room</b> является комнатой IRC, в которую вы хотите войти, <b>irc.domain.tld</b> это адрес IRC-сервера, к которому вы хотите присоединиться. Не забудьте оставить <b>@irc.disroot.org</b> без изменений, потому что это адрес шлюза IRC.</p>"
    -
        question: "Я не могу подключиться к Diaspora* со своей учетной записью Disroot."
        answer: "<p>Посколькку <b>Diaspora*</b> не поддерживает <b>LDAP</b> (программный протокол, который мы используем для доступа к службам с одной учетной записью) невозможно войти с помощью  <b>Disroot</b>, поэтому необходимо создать другую учетную запись <a href='https://pod.disroot.org/' target='_blank'>тут</a>. Однако в настоящее время регистрация закрыта.</p>"

---
