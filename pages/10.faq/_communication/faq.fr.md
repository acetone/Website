---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMMUNICATION"
section_number: "100"
faq:
    -
        question: "Je n'arrive pas à me connecter à Matrix. Pourquoi ?"
        answer: "<p>Nous avons décidé d'arrêter <b>Matrix</b> depuis septembre 2018. Vous pouvez lire les raisons <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>ici</a>.</p>
        <p>Vous pouvez également trouver plus d'informations sur l'impact de Matrix sur la vie privée <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>ici</a></p>"
    -
        question: "Comment utiliser l'IRC de Disroot dans les salles XMPP ?"
        answer: "<p><b>Disroot</b> fournit ce qu'on appelle une passerelle IRC. Cela signifie que vous pouvez rejoindre n'importe quelle salle IRC hébergée sur n'importe quel serveur IRC. Il s'agit du schéma d'adresse de base :</p>
        <ol>
        <li>Pour rejoindre une salle IRC: <b>#salle%irc.domain.tld@irc.disroot.org</b></li>
        <li>Pour ajouter un contact IRC: <b>nomducontact%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Où <b>#salle</b> est la salle IRC que vous voulez rejoindre et <b>irc.domain.tld</b> est l'adresse du serveur irc que vous voulez rejoindre. Assurez-vous de laisser <b>@irc.disroot.org</b> tel quel car c'est l'adresse des passerelles IRC.</p>"
    -
        question: "Je n'arrive pas à me connecter à Diaspora* avec mon compte Disroot."
        answer: "<p>Comme <b>Diaspora*</b> n'est pas compatible avec <b>LDAP</b> (le protocole logiciel que nous utilisons pour accéder aux services avec un seul compte), il n'est pas possible de se connecter avec les identifiants <b>Disroot</b>. Vous devez créer un compte différent <a href='https://pod.disroot.org/' target='_blank'>ici</a>. Cependant, les inscriptions sont actuellement fermées.</p>"

---
