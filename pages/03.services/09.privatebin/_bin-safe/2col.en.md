---
title: 'Safe'
wider_column: left
---

# Safe and secure
<ul class=disc>
<li>The data is compressed and encrypted in the browser before it's sent to the server, using 256 bits AES. Server has zero knowledge of data being stored, including nicknames and comments.</li>
<li>Thanks to encryption, your data is safe even in case of server breach or seizure.</li>
<li>Search engines are blind regarding paste content.</li>
<li>Discussion is also encrypted/decrypted in the browser.</li>
<li>With paste expiration, you can have ad-hoc short-lived discussion which will disappear in the void after expiration. This will leave no trace of your discussions in your email boxes.</li>
<li>Discussions or paste content cannot be indexed by search engines.</li>
</ul>
---

![](en/privatebin02.gif)
