---
title: 'Sécurisé'
wider_column: left
---

# Sûr et sécurisé
<ul class=disc>
<li>Données compressées et cryptées dans le navigateur avant l'envoi au serveur, en utilisant 256 bits AES. Le serveur n'a aucune connaissance des données stockées. </li>
<li> Vos données sont en sécurité même en cas de violation ou de saisie du serveur.</li>
<li>Les moteurs de recherche sont aveugles en ce qui concerne le contenu de la pâte.</li>
<li>Discussion est également crypté/décrypté dans le navigateur.</li>
<li>Le serveur ne peut pas voir le contenu des commentaires ni les surnoms.</li>
<li>Avec l'expiration du collage, vous pouvez avoir une discussion ad-hoc de courte durée qui disparaîtra dans le vide après l'expiration. Cela ne laissera aucune trace de vos discussions dans vos boîtes email.</li>
<li>Les discussions ou le contenu collé ne peuvent pas être indexés par les moteurs de recherche.</li>
</ul>

---

![](en/privatebin02.gif)
