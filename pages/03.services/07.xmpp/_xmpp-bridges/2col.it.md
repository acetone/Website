---
title: 'Xmpp Bridges'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations.jpg)

---

## Gateway, Bridge e protocolli di trasporto

Jabber/XMPP consente vari modi per connettersi a diversi protocolli di chat. È possibile connettersi a qualsiasi room IRC tramite il nostro gateway IRC biboumi. In futuro stiamo progettando di eseguire altri bridge: Matrix (attualmente disponibile presso gli amici di bau-haus.net), Telegram e altri ancora.
