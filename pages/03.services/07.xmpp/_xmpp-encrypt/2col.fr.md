---
title: 'Chiffrer xmpp'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Chiffrez tout!

En utilisant, du côté client (càd. de votre côté) et d'un bout à l'autre, les méthodes de chiffrage OMEMO (GPG ou OTR), vos conversations et messages parviendront à leur destinataire sans pouvoir être interceptés par qui que ce soit (même pas les admins).

---

![](omemo_logo.png)
