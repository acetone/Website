---
title: 'Xmpp Verschlüsselung'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Verschlüssele alles!

Durch die Nutzung der OMEMO (GPG oder OTR) Ende-zu-Ende-Verschlüsselungsmethoden erreichen Eure Unterhaltungen den Nachrichtenempfänger, ohne durch irgendjemand abgefangen zu werden (nicht mal durch Administratoren).

---

![](omemo_logo.png)
