---
title: CryptPad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Inscribirse</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Crear Documento</a>

---
![](cryptpad_logo.png)

## CryptPad
CryptPad de Disroot está desarrollado por **CryptPad** y proporciona una suite colaborativa de ofimática totalmente [cifrada de extremo-a-extremo](https://es.wikipedia.org/wiki/Cifrado_de_extremo_a_extremo). Te permite crear, compartir y trabajar con otras personas en documentos de texto, hojas de cálculo, presentaciones, pizarras, incluso organizar tu proyecto en un tablero [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)). Todo ello con cero conocimiento, ya que los datos son cifrados antes de dejar tu computadora.

CryptPad de Disroot: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Sitio del Proyecto: [https://cryptpad.fr](https://cryptpad.fr)

Código fuente: [https://github.com/xwiki-labs/cryptpad](https://github.com/xwiki-labs/cryptpad)
