---
title: 'Partager un sondage'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](poll_share.gif)

---

# Partage de sondage
Partagez votre sondage avec les participants en leur envoyant un lien.
