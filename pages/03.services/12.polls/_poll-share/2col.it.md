---
title: 'Condividi il sondaggio'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](poll_share.gif)

---

# Condividi il sondaggio
