---
title: 'Créer un sondage'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Créer un sondage
Possibilité de créer un sondage spécifique à une date ou un type de sondage plus général.

---

![](en/poll_create.gif)
