---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Utilisez un chat textuel en plus de votre conférence audio/vidéo.

---

![](chat.png)
