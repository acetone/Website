---
title: "Caractéristiques d'Etherpad"
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Surlignage en couleur

Tous les changements dans le texte sont marqués avec la couleur d'auteur assignée à chaque utilisateur dans le pad.

## Style du texte

Gras, italique, paragraphes, listes, listes de points, etc.

## Chat

Chat en ligne pour tous les utilisateurs travaillant sur le texte.

## Historique

Passez en revue les changements avec un curseur temporel.

---

![](en/etherpad-example.png?lightbox=1024)
