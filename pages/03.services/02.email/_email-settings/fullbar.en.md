---
title: 'Email Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">993</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Port <span style="color:#8EB726">587</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Port <span style="color:#8EB726">465</span> | Authentication: <span style="color:#8EB726">Normal Password</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">995</span> | Authentication: <span style="color:#8EB726">Normal Password</span>

---

**Mailbox size limit:** 1GB
**Attachement size limit:** 50MB

---

**Delimiters:** You can use delimiters (plus sign, "+") in your email address to create sub-addresses, like **username+whatever@disroot.org**, for example for filtering and tracking spam. Example: david@disroot.org can set up mail addresses like 'david+bank@disroot.org' that he could give to his bank. It can be used **only to receive emails**, not to send ones. Your emails will always be sent as david@disroot.org.
