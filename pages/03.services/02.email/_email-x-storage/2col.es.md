---
title: 'Espacio Extra para el buzón'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Aumenta el espacio de tu buzón de correo

Es posible aumentar el espacio de almacenamiento de tu E-mail hasta 10 GB por 0.15 euro por GB mensual, abonado anualmente.

Para solicitar espacio extra necesitas completar [este formulario](https://disroot.org/es/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/es/forms/extra-storage-mailbox">Solicitar espacio adicional para el buzón</a>
