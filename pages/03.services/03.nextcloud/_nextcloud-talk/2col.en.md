---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: Talk
        logo: en/talk_logo.png
        link: https://f-droid.org/en/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Audio/video conference calls. Securely communicate with your friends and family using rich audio and video from your browser.  All calls are peer 2 peer utilizing WebRTC technology so the only bottleneck is your local internet speed rather than **Disroot**'s server resources.

You can create a public link that you can share with people that don't have a **Nextcloud** account.

---

![](en/call-in-action.png?lightbox=1024)
