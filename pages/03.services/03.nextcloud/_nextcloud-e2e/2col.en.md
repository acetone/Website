---
title: 'End to End'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/endtoend-android-nw.png?lightbox=1024)

---

## End-to-End Encryption

**Nextcloud End-to-End encryption** offers the ultimate protection for your data. **Nextcloud**'s E2E encryption plugin enables users to pick one or more folders on their desktop or mobile client to be encrypted using End-to-End encryption. Folders can be shared with other users and synced between devices but are not readable on the server side.

Read more about it at [https://nextcloud.com/endtoend/](https://nextcloud.com/endtoend/)

<span style="color:red">**End-to-end encryption is still in alpha state, don't use this in production and only with test data!**</span>

### ATTENTION!

Currently, Nextcloud end-to-end encryption is disabled on Disroot. It is due to a long standing bug with Nextcloud desktop app.

Read about those issues:

[Can't revoke keys](https://github.com/nextcloud/end_to_end_encryption/issues/32)

[Directory structure is leaked](https://github.com/nextcloud/end_to_end_encryption/issues/94)

[Uploding e2e encrypted files](https://github.com/nextcloud/desktop/issues/890)
