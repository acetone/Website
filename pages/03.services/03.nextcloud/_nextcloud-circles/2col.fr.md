---
title: 'Cercles Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-circles.png?lighbox=1024)

---

## Cercles

Créez vos propres groupes d'utilisateurs/collègues/amis pour partager facilement du contenu. Vos cercles peuvent être publics, privés (nécessite d'inviter bien que visible aux autres) ou secrets (nécessite mot de passe et invisible).
