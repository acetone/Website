---
title: 'Caractéristiques de Taiga'
wider_column: left
---

## Tableau Kanban

Pour ceux qui ne sont pas familiers avec les outils de gestion de projet et de scrum, nous vous conseillons de choisir un modèle Kanban simple. (A faire > En cours > Terminé).  [https://en.wikipedia.org/wiki/Kanban](https://en.wikipedia.org/wiki/Kanban)


## Projet Scrum

Avec un backlog, une planification par sprint, des points par "User Story" et tous les autres goodies dont chaque projet Scrum a besoin [Développement du logiciel](https://fr.wikipedia.org/wiki/Scrum_(d%C3%A9veloppement))


## Tableau des problèmes

Lieu où vous pouvez rassembler toutes les questions, suggestions, problèmes, etc. et travailler collectivement pour les résoudre.


## Wiki

Documentez votre travail avec le wiki intégré.


## Epics
Regrouper les tâches en "Epics" que vous pouvez réaliser au fil du temps, même sur plusieurs projets.

---

![](en/taiga-backlog.png?lightbox=1024)

![](en/taiga-kanban.png?lightbox=1024)
