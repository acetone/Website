---
title: Mission Statement
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### TL:DR (Too Long, DID Read)<br>(*Très long ; Je l'ai lu*)

Le but de Disroot.org est de récupérer le monde virtuel de la réalité monolithique qui l'a accaparé. Nous sommes convaincus que cette approche a un potentiel gigantesque pour arracher la société au statu quo actuel régi par la publicité, le consumérisme et le pouvoir du plus offrant. En décentralisant un réseau de services coopératifs au XXIe siècle, nous pouvons surmonter les lacunes imposées par les limitations financières, l'empreinte énergétique et la complexité des infrastructures - en donnant au réseau le pouvoir de croître de façon autonome selon ses propres règles. Un réseau de nombreuses petites tribus coopérant les unes avec les autres vaincra un géant monolithique. Viendrez-vous vous "déraciner" avec nous ?
