---
title: Leitbild
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

## <br><br>
<span style="float:right;">
![](disroot-root.png)
</span>

---

## Die Mission der Disroot.org-Stiftung

<br>
Die Mission, die sich die Disroot.org-Stiftung gesetzt hat, zielt darauf ab, das Internet als das faccettenreiche, unabhängige Werkzeug, als das es immer gedacht war, zurückzubringen.

Das einst dezentralisierte, demokratische und freie Internet wird nun schon lange von einer Handvoll Technologie-Giganten beherrscht, die eine Zentralisierung in Monopolen, mehr staatliche Kontrolle und restriktivere Regeln fördern. Nach unserer Meinung widerspricht und zerstört all das die wahre Seele dieses wundervollen Werkzeugs.

Durch die Förderung freier und quelloffener Software unterstützen wir kollektive und gemeinschaftliche Bemühungen zur Zusammenarbeit. Disroot.org möchte dabei ein Beispiel dafür sein, dass es mögliche Alternativen gibt zur unternehmensgesteuerten Welt, die da draußen vorherrscht. Alternativen, die nicht erstellt wurden mit dem Ziel, ökonomischen Nutzen daraus zu ziehen. Alternativen, für die Transparenz, Offenheit, Toleranz und Respekt Schlüsselelemente sind.

### Die Plattform

Disroot.org bietet eine Plattform für Webdienste, die auf den Prinzipien von Datenschutz, Offenheit, Transparenz, und Freiheit basieren. Das wichtigste Ziel dieser Plattform aus technischer Sicht ist es, den bestmöglichen Service mit dem besten Support zu bieten. Dafür haben wir den Ansatz gewählt, in welchem die Nutzer (im Folgenden als Disrooter bezeichnet) der wertvollste Bestandteil und die größten Nutznießer des Projekts sind; Entscheidungen werden in ihrem Sinn getroffen und nicht mit dem Ziel, finanzielle Vorteile, persönliche Erfüllung Kontrolle oder Macht zu erlangen. Disroot.org hat sich verpflichtet, diese Grundprinzipien auf die bestmögliche Weise zu verteidigen.

Der Online-Datenschutz wird immer wieder aufgeweicht durch jene, die auf der Jagd nach Profit und finanzieller Kontrolle sind. Unsere Daten sind zu einer sehr wertvollen Ware geworden und die meisten Online-Unternehmen nutzen das offen oder verdeckt zu ihrem Vorteil. Disroot.org hat sich verpflichtet, niemals die Informationen, die von Disrootern übermittelt wurden, für finanzielle, politische oder machtstrebende Zwecke an Dritte zu verkaufen, bei der Verarbeitung zu helfen oder sie zu nutzen. Die Daten der Disrooter gehören ihnen und das Projekt lagert diese für sie. Unser Motto lautet: "Je weniger wir über unsere Nutzer wissen umso besser". Wir implementieren Daten-Verschlüsselung wo immer das möglich ist um sicherzustellen, dass es für nicht autorisierte Dritte so schwer wie möglich ist, an Nutzerdaten zu gelangen, und wir behalten auch nur das absolute Minimum an Nutzer-Logs und -Daten, das essentiell für die Performance unserer Dienste ist.
