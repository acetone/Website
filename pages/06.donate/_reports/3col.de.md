---
title: 'Jährliche Berichte'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

# Jahreserichte:

**Jahresbericht 2018** <br>
[Bericht 2018](https://disroot.org/annual_reports/AnnualReport2018.pdf)

**Aug 2016 - Aug 2017** <br>
[Bericht 2016-2017](https://disroot.org/annual_reports/2017.pdf)

**2015 - Aug 2016** <br>
Kosten: €1569.76 <br>
Spenden: €264.52

---



---
<br>

# Hardware-Spenden
Wenn Du irgendwelche Hardware hast, die für dieses Projekt genutzt werden kann (Speicher, Server, CPUs, Festplatten, Raid-/Netzwerk-Karten, Netzwerkausstattung etc.), melde Dich bitte bei uns. Jede Hardware ist hilfreich. Was wir nicht brauchen, können wir verkaufen. Wir freuen uns außerdem über Angebote von Server-Racks.
