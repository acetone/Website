---
title: Overview
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Situazione finanziaria

![](graph.png?lightbox=1024)


Supportato da <button class="button4">207</button> Disrooters
_(Click the number for an overview)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)
